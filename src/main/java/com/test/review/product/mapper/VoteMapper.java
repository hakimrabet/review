package com.test.review.product.mapper;

import com.test.review.product.model.Vote;
import com.test.review.product.payload.VoteRequest;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface VoteMapper {

    VoteMapper INSTANCE = Mappers.getMapper(VoteMapper.class);

    Vote toVote(VoteRequest request);
}
