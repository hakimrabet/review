package com.test.review.product.model;

import com.test.review.base.model.BaseEntity;
import com.test.review.user.model.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "t_comment")
@Entity
@Setter
@Getter
public class Comment extends BaseEntity {

    @Column(nullable = false, columnDefinition = "TEXT")
    private String text;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;
}
