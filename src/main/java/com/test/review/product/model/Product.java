package com.test.review.product.model;

import com.test.review.base.model.BaseEntity;
import com.test.review.provider.model.Provider;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Table(name = "t_product")
@Entity
@Setter
@Getter
public class Product extends BaseEntity {

    @Column(length = 50, nullable = false)
    private String name;

    @Column(nullable = false)
    @Min(0)
    private long price;

    @Column(nullable = false)
    private boolean visible = true;

    @Column(nullable = false)
    private boolean comment = true;

    @Column(nullable = false)
    private boolean vote = true;

    @Column(nullable = false)
    private boolean forBuyerOnly = false;

    @ManyToOne
    @JoinColumn(name = "provider_id")
    private Provider provider;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "product")
    private Set<Comment> comments;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "product")
    private Set<Vote> votes;

}
