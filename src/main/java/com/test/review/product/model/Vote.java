package com.test.review.product.model;

import com.test.review.base.model.BaseEntity;
import com.test.review.product.constant.VoteOption;
import com.test.review.user.model.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "t_vote")
@Entity
@Setter
@Getter
public class Vote extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "vote", nullable = false, length = 30)
    private VoteOption vote;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;
}
