package com.test.review.product.payload;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductRequest implements Serializable {

    private String name;
    private long price;
    private boolean visible;
    private boolean comment;
    private boolean vote;
    private boolean forBuyerOnly;

}
