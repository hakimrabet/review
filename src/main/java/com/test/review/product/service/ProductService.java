package com.test.review.product.service;

import com.test.review.product.mapper.CommentMapper;
import com.test.review.product.mapper.ProductMapper;
import com.test.review.product.model.Comment;
import com.test.review.product.model.Product;
import com.test.review.product.payload.AddProductRequest;
import com.test.review.product.payload.CommentRequest;
import com.test.review.product.payload.EditProductRequest;
import com.test.review.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product findById(String id) {
        return productRepository.findById(id).get();
    }

    public Product saveProduct(AddProductRequest request) {
        return productRepository.save(ProductMapper.INSTANCE.toProduct(request));
    }

    public Product updateProduct(String id, EditProductRequest request) {
        Product dbProduct = findById(id);
        ProductMapper.INSTANCE.update(dbProduct, request);
        return productRepository.save(dbProduct);
    }

    public Page<Product> getProducts(int page, int size) {
        return productRepository.findAll(PageRequest.of(page, size));
    }

    public Void deleteProduct(String id) {
        productRepository.deleteById(id);
        return null;
    }

    public Page<Product> getProductsByVisible(boolean visibility, int page, int size) {
        return productRepository.findProductsByVisible(visibility, PageRequest.of(page, size));
    }

    public Product findByIdAndVisible(boolean visibility, String id) {
        //TODO: must be orElseThrow
        return productRepository.findByIdAndVisible(visibility, id).get();
    }

    public Comment addComment(String postId, CommentRequest request) {
        Product product = productRepository.findById(postId).get();
        Comment comment = CommentMapper.INSTANCE.toComment(request);
        productRepository.save(product);
        return comment;
    }

    public Page<Comment> getCommentsByVisible(boolean visibility, int page, int size) {
        return productRepository.findCommentsByVisible(visibility, PageRequest.of(page, size));
    }

}
