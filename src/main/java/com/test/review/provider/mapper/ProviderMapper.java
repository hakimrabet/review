package com.test.review.provider.mapper;

import com.test.review.provider.model.Provider;
import com.test.review.provider.payload.AddProviderRequest;
import com.test.review.provider.payload.EditProviderRequest;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProviderMapper {

    ProviderMapper INSTANCE = Mappers.getMapper(ProviderMapper.class);

    Provider toProvider(AddProviderRequest request);

    Provider toProvider(EditProviderRequest request);

    void update(@MappingTarget Provider entity, EditProviderRequest updateEntity);

}
