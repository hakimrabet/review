package com.test.review.provider.model;

import com.test.review.base.model.BaseEntity;
import com.test.review.product.model.Product;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Table(name = "t_provider")
@Entity
@Setter
@Getter
public class Provider extends BaseEntity {

    @Column(length = 30, nullable = false)
    private String name;

    @OneToMany(mappedBy = "provider", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Product> product;

}
