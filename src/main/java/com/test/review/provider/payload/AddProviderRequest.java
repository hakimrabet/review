package com.test.review.provider.payload;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AddProviderRequest extends ProviderRequest {
}
