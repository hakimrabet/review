package com.test.review.provider.service;

import com.test.review.provider.mapper.ProviderMapper;
import com.test.review.provider.model.Provider;
import com.test.review.provider.payload.AddProviderRequest;
import com.test.review.provider.payload.EditProviderRequest;
import com.test.review.provider.repository.ProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ProviderService {

    private final ProviderRepository providerRepository;

    @Autowired
    public ProviderService(ProviderRepository providerRepository) {
        this.providerRepository = providerRepository;
    }

    public Provider findById(String id) {
        return providerRepository.findById(id).get();
    }

    public Provider save(AddProviderRequest request) {
        return providerRepository.save(ProviderMapper.INSTANCE.toProvider(request));
    }

    public Provider update(String id, EditProviderRequest request) {
        Provider dbProvider = findById(id);
        ProviderMapper.INSTANCE.update(dbProvider, request);
        return providerRepository.save(dbProvider);
    }

    public Page<Provider> getProviders(int page, int size) {
        return providerRepository.findAll(PageRequest.of(page, size));
    }

    public Void deleteProvider(String id) {
        providerRepository.deleteById(id);
        return null;
    }

}
