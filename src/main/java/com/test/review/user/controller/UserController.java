package com.test.review.user.controller;

import com.test.review.user.model.User;
import com.test.review.user.payload.AddUserRequest;
import com.test.review.user.payload.EditUserRequest;
import com.test.review.user.service.UserService;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<Page<User>> browse(@RequestParam(defaultValue = "0") @Range(min = 0) int page,
                                             @RequestParam(defaultValue = "20") @Range(min = 1, max = 20) int size) throws Throwable {
        return ResponseEntity.ok(userService.getUsers(page, size));
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> read(@PathVariable("id") String id) throws Throwable {
        return ResponseEntity.ok(userService.findById(id));
    }

    @PostMapping()
    public ResponseEntity<User> create(@Validated @RequestBody AddUserRequest request) throws Throwable {
        return ResponseEntity.ok(userService.save(request));
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> update(@PathVariable("id") String id, @RequestBody EditUserRequest request) throws Throwable {
        return ResponseEntity.ok(userService.update(id, request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") String id) throws Throwable {
        return ResponseEntity.ok(userService.deleteUser(id));
    }

}
