package com.test.review.user.payload;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserRequest implements Serializable {

    @Size(min = 1, max = 50)
    @NotNull(message = "firstname is neccessary")
    private String firstName;

    @Size(min = 1, max = 50)
    @NotNull(message = "lastname is neccessary")
    private String lastName;
}
