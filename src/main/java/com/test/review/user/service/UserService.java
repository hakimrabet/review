package com.test.review.user.service;

import com.test.review.user.mapper.UserMapper;
import com.test.review.user.model.User;
import com.test.review.user.payload.AddUserRequest;
import com.test.review.user.payload.EditUserRequest;
import com.test.review.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    static {


    }
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findById(String id) {
        return userRepository.findById(id).get();
    }

    public User save(AddUserRequest request) {
        return userRepository.save(UserMapper.INSTANCE.toUser(request));
    }

    public User update(String id, EditUserRequest request) {
        User dbUser = findById(id);
        UserMapper.INSTANCE.update(dbUser, request);
        return userRepository.save(dbUser);
    }

    public Page<User> getUsers(int page, int size) {
        return userRepository.findAll(PageRequest.of(page, size));
    }

    public Void deleteUser(String id) {
        userRepository.deleteById(id);
        return null;
    }

}
